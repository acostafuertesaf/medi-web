-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.14-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para medi_web
CREATE DATABASE IF NOT EXISTS `medi_web` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */;
USE `medi_web`;

-- Volcando estructura para tabla medi_web.categorias
CREATE TABLE IF NOT EXISTS `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.categorias: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
REPLACE INTO `categorias` (`id_categoria`, `descripcion`, `estado`) VALUES
	(1, 'Analgésicos', b'1'),
	(2, 'Antiácidos', b'1'),
	(3, 'Antialérgicos', b'1'),
	(4, 'Antidiarreicos', b'1'),
	(5, 'Antiinfeciosos', b'1'),
	(6, 'Antiinflamatorios', b'1'),
	(7, 'Antitipirécos', b'0'),
	(8, 'Mucolíticos', b'1'),
	(9, 'Bebés', b'1');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `correo_electronico` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  `codigo` varchar(50) NOT NULL,
  PRIMARY KEY (`id_cliente`),
  KEY `Índice 2` (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.clientes: ~37 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
REPLACE INTO `clientes` (`id_cliente`, `nombre`, `apellido`, `correo_electronico`, `fecha_nacimiento`, `direccion`, `estado`, `codigo`) VALUES
	(1, 'Prueba', 'Prueba', 'prueba@gmail.com', '2020-10-22', 'prueba', b'1', ''),
	(2, 'Andres felipe', 'fuertes', 'acostafuertesaf@gmail.com', '2020-10-05', 'cr 24 a # 59 b 49', b'1', ''),
	(3, 'adfadsf', 'asdfasdf', 'acostafuertesaf@gmail.com', '2020-10-14', 'cr 24 a # 59 b 49', b'1', ''),
	(4, 'felipe', 'felipe', 'felipe@gmail.com', '2020-10-06', 'felipe', b'1', ''),
	(5, 'alejo', 'alejo', 'alejo@gmail.com', '2020-10-12', 'alejo', b'1', ''),
	(6, 'jose', 'jose', 'jose@gmail.com', '2020-10-05', 'jose', b'1', ''),
	(7, 'jjojo', 'jjojo', 'jjojo@gmail.com', '2020-10-08', 'jjojo', b'1', ''),
	(8, 'fdfd', 'fdfd', 'fdfd@gmail.com', '2020-10-21', 'fdfd', b'1', ''),
	(9, 'tyty', 'tyty', 'tyty@gmail.com', '2020-10-13', 'tyty', b'1', ''),
	(10, 'felongas', 'felongas', 'felongas@gmail.com', '2020-10-14', 'felongas', b'1', ''),
	(11, 'hyhy', 'hyhy', 'hyhy', '2020-10-08', 'hyhy', b'1', ''),
	(12, 'ttrtrt', 'trtrt', 'trtrtgmail.com', '2020-10-07', 'trt', b'1', ''),
	(13, 'rere', 'rere', 'rere@gmail.com', '2020-10-06', 'rere', b'1', ''),
	(14, 'alejito', 'alejito', 'alejito@gmail.com', '2020-10-05', 'alejito', b'1', ''),
	(15, 'rttr', 'trtr', 'trtr@es.com', '2020-10-13', 'trtr', b'1', ''),
	(16, 'aas', 'aas', 'aas@gmail.com', '2020-10-13', 'aaass', b'1', ''),
	(17, 'carmelo', 'carmelo', 'carmelo@gmail.com', '2020-10-06', 'cr 24 a # 59 b 49', b'1', '36390603-b657-4290-86d8-d7762aebb0e6'),
	(18, 'carmelo', 'carmelo', 'carmelo@gmail.com', '2020-10-06', 'cr 24 a # 59 b 49', b'1', '36390603-b657-4290-86d8-d7762aebb0e6'),
	(19, 'gghh', 'gghh', '$gghh@gmail.com', '2020-10-30', 'calle 45 b 65 7', b'1', '28d948b9-9831-4ade-9f17-d777cdc465d7'),
	(20, 'gghh', 'gghh', '$gghh@gmail.com', '2020-10-30', 'fgsdfgdfg', b'1', '28d948b9-9831-4ade-9f17-d777cdc465d7'),
	(21, 'gghh', 'gghh', '$gghh@gmail.com', '2020-10-30', 'fgsdfgdfg', b'1', '28d948b9-9831-4ade-9f17-d777cdc465d7'),
	(22, 'ff', 'ff', 'acostafuertesaf@gmail.com', '2020-09-29', 'cr 24 a # 59 b 49', b'1', '6d7cd65a-d6fd-4886-829f-c590f6f56f31'),
	(23, 'yyyyyyy', 'yyyyyy', 'yyyy@gmail.com', '2020-10-13', 'cr 24 a # 59 b 49', b'1', 'b38d8105-dad6-4d5e-9ba7-5ca8d16b1d58'),
	(24, 'gghh', 'gghh', '$gghh@gmail.com', '2020-10-30', 'fgsdfgdfg', b'1', '28d948b9-9831-4ade-9f17-d777cdc465d7'),
	(25, 'dfghdfgh', 'dfghdfgh', 'acostafuertesaf@gmail.com', '2020-09-28', 'cr 24 a # 59 b 49', b'1', '83aa56ee-ba5c-496b-adf6-236ca324c5f0'),
	(26, 'qqqq', 'qqqqq', 'qqqq@gmail.com', '2020-10-31', 'cr 24 a # 59 b 49', b'1', '71018d7d-3364-433c-9c99-ceb8daec54d6'),
	(27, 'aaaaa', 'aaaa', 'aaaa@gmail.com', '2020-10-31', 'cr 24 a # 59 b 49', b'1', '8920e16d-9ac4-4971-8185-86a4d678ba14'),
	(28, 'llllll', 'lllll', 'lllll@gmail.com', '2020-10-22', 'cr 24 a # 59 b 49', b'1', '9fdfc8e8-4891-4b44-8741-6e9754359751'),
	(29, 'llllll', 'lllll', 'lllll@gmail.com', '2020-10-22', 'cr 24 a # 59 b 49', b'1', '9fdfc8e8-4891-4b44-8741-6e9754359751'),
	(30, 'cccc', 'cccc', 'acostafuertesaf@gmail.com', '2020-09-28', 'cr 24 a # 59 b 49', b'1', '0539d653-bf71-4a95-a650-e7cdce3bbef8'),
	(31, 'cccc', 'cccc', 'acostafuertesaf@gmail.com', '2020-09-28', 'cr 24 a # 59 b 49', b'1', '0539d653-bf71-4a95-a650-e7cdce3bbef8'),
	(32, 'zzzz', 'zzzz', 'zzzz@gmail.com', '2020-10-13', 'cr 24 a # 59 b 49', b'1', '580d0bbc-ab43-493a-8002-2a11671e09d6'),
	(33, 'prueba', 'prueba', 'prueba@gmail.com', '2020-10-15', 'cr 24 a # 59 b 49', b'1', 'e2671059-1213-4dd5-b3df-7f5cc1e1675a'),
	(34, 'tyty', 'tyty', 'acostafuertesaf@gmail.com', '2020-10-05', 'cr 24 a # 59 b 49', b'1', 'e5f01b5b-b355-488c-9a79-1a80ed93b217'),
	(35, 'wewe', 'wewe', 'acostafuertesaf@gmail.com', '2020-10-06', 'cr 24 a # 59 b 49', b'1', '53d9e1c3-b99c-4e2b-80e0-b8d6ed845a7f'),
	(36, 'rerre', 'rere', 'acostafuertesaf@gmail.com', '2020-10-05', 'cr 24 a # 59 b 49', b'1', 'f0f9d55e-a92b-41c4-9494-dcde23eef509'),
	(37, 'ewew', 'ewew', 'acostafuertesaf@gmail.com', '2020-10-13', 'cr 24 a # 59 b 49', b'1', 'be90f14f-7548-4ca6-94d3-79ac4ebc0834'),
	(38, 'prueba', 'prueba', 'acostafuertesaf@gmail.com', '2020-09-28', 'cr 24 a # 59 b 49', b'1', 'dd55ad92-f4c4-41a3-8f5c-8b2e04ff0d20'),
	(39, 'prueba final', 'prueba final', 'finl@gmail.com', '2020-10-06', 'cr 24 a # 59 b 49', b'1', 'f02f5095-c437-4471-94ba-3228f92bacda'),
	(40, 'qqqq', 'qqqq', 'qqq@gmail.com', '2020-10-05', 'qqqq', b'1', '40102bb5-ff14-4346-ad42-292a9d0d0c8c'),
	(41, 'ana maria', 'sierra', 'ana@gmail.com', '2020-10-06', 'cr 24 a # 59 b 49', b'1', 'c93b2614-8730-4387-9a99-412a2793ec37');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.empleados
CREATE TABLE IF NOT EXISTS `empleados` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `documento` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `correo_electronico` varchar(100) NOT NULL,
  `tipo_empleado` int(11) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_empleado`),
  KEY `FK_empleados_tipo_empleados` (`tipo_empleado`),
  CONSTRAINT `FK_empleados_tipo_empleados` FOREIGN KEY (`tipo_empleado`) REFERENCES `tipo_empleados` (`id_tipo_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.empleados: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `empleados` DISABLE KEYS */;
REPLACE INTO `empleados` (`id_empleado`, `documento`, `nombre`, `apellido`, `fecha_nacimiento`, `correo_electronico`, `tipo_empleado`, `estado`) VALUES
	(2, '987654321', 'Jose', 'Gomez', '2020-10-21', 'jose@gmail.es', 1, b'1'),
	(7, '123456123', 'Ana', 'Mejia', '2020-10-24', 'ana@gmail.com', 2, b'0'),
	(11, '123456789', 'Andres', 'Acosta', '2020-10-15', 'acostafuertesaf@gmail.com', 2, b'1'),
	(12, '147258369', 'Maria', 'Serna', '2020-10-05', 'maria@gmail.com', 2, b'1'),
	(13, '123', 'prueba', 'prueba', '2020-10-31', 'prueba@gmail.com', 1, b'1');
/*!40000 ALTER TABLE `empleados` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.farmacias
CREATE TABLE IF NOT EXISTS `farmacias` (
  `id_farmacia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_farmacia`),
  KEY `FK__municipios` (`id_municipio`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.farmacias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `farmacias` DISABLE KEYS */;
REPLACE INTO `farmacias` (`id_farmacia`, `nombre`, `descripcion`, `direccion`, `id_municipio`, `estado`) VALUES
	(1, 'Farmaplus', 'Farmacia de sabaneta', 'carrera 47 nro 76DSur-34', 1, b'1');
/*!40000 ALTER TABLE `farmacias` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.laboratorios
CREATE TABLE IF NOT EXISTS `laboratorios` (
  `id_laboratorio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_laboratorio`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.laboratorios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `laboratorios` DISABLE KEYS */;
REPLACE INTO `laboratorios` (`id_laboratorio`, `descripcion`, `estado`) VALUES
	(1, 'Genfar', b'1');
/*!40000 ALTER TABLE `laboratorios` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.medicamentos
CREATE TABLE IF NOT EXISTS `medicamentos` (
  `id_medicamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_comercial` varchar(50) NOT NULL,
  `nombre_generico` varchar(50) NOT NULL,
  `descripcion` tinytext NOT NULL,
  `precio` double(22,0) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `id_farmacia` int(11) NOT NULL,
  `id_laboratorio` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_medicamento`) USING BTREE,
  KEY `Índice 2` (`id_farmacia`),
  KEY `Índice 3` (`id_laboratorio`),
  KEY `Índice 4` (`id_categoria`),
  CONSTRAINT `FK_medicamentos_categorias` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`),
  CONSTRAINT `FK_medicamentos_farmacias` FOREIGN KEY (`id_farmacia`) REFERENCES `farmacias` (`id_farmacia`),
  CONSTRAINT `FK_medicamentos_laboratorios` FOREIGN KEY (`id_laboratorio`) REFERENCES `laboratorios` (`id_laboratorio`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.medicamentos: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `medicamentos` DISABLE KEYS */;
REPLACE INTO `medicamentos` (`id_medicamento`, `nombre_comercial`, `nombre_generico`, `descripcion`, `precio`, `imagen`, `fecha_vencimiento`, `id_farmacia`, `id_laboratorio`, `stock`, `id_categoria`, `estado`) VALUES
	(3, 'Acetaminofen', 'Paracetamol', 'calmantes para el dolor y antipiréticos para bajar la fiebre. Funciona al cambiar la forma en que el cuerpo siente el dolor y por el enfriamiento del cuerpo.', 1000, 'images/acetaminofe.jpg', '2023-05-10', 1, 1, 16, 1, b'1'),
	(4, 'Diclofenaco', 'Voltaren', 'Está indicado para reducir inflamaciones y como analgésico. Se puede usar para reducir los cólicos menstruales.', 2400, 'images/Analgesicos/Diclofenaco.jpg', '2021-08-12', 1, 1, 5, 1, b'1'),
	(6, 'Azitromicina', 'Zitromax', 'antibiótico de amplio espectro que pertenece al grupo de los macrólidos, posee acción sobre bacterias Gram +, Gram -', 4000, 'images/Analgesicos/Azitromicina.jpg', '2020-10-29', 1, 1, 128, 1, b'1'),
	(7, 'Dipirona', 'metamizol', 'Es un fármaco perteneciente a la familia de las pirazolonas, cuyo prototipo es el piramidón. También se le conoce como dipirona', 11650, 'images/Analgesicos/Dipirona.jpg', '2020-10-31', 1, 1, 198, 1, b'1'),
	(8, 'Propanolol', 'Propanolol', 'Usado principalmente en el tratamiento de la hipertensión.', 5000, 'images/Analgesicos/Propanolol.jpg', '2021-04-15', 1, 1, 98, 1, b'1'),
	(9, 'Ibuprofeno', 'Ibuprofeno', 'Medicamento que se usa para tratar la fiebre, la hinchazón, el dolor y el enrojecimiento al impedir que el cuerpo elabore sustancias que causan inflamación.', 2000, 'images/Analgesicos/Ibuprofeno.jpg', '2021-05-20', 1, 1, 99, 1, b'1'),
	(10, 'Almax', 'Almax', 'Está indicado en el alivio y tratamiento sintomático de la acidez y ardor de estómago en adultos y mayores de 12 años.', 6500, 'images/Antiacidos/Almax.jpg', '2021-05-14', 1, 1, 87, 2, b'1'),
	(11, 'Carbonato de Calcio', 'Carbonato de Calcio', 'Se usa como un antiácido para aliviar los síntomas de indigestión o acidez estomacal.', 9800, 'images/Antiacidos/Carbonato.jpg', '2021-07-15', 1, 1, 55, 2, b'1'),
	(12, 'Gaviscon', 'Gaviscon', 'Forma una barrera protectora en el estómago para evitar el reflujo gástrico calmando el ardor en la boca del estómago.', 6500, 'images/Antiacidos/Gaviscon.png', '2020-10-09', 1, 1, 64, 2, b'1'),
	(13, 'Hidroxido de Aluminio', 'Hidroxido de Aluminio', 'Medicamento para la acidez gástrica que contienen otros ingredientes activos para reducir la acidez.', 7800, 'images/Antiacidos/Hidroxido de aluminio.jpg', '2020-10-23', 1, 1, 34, 2, b'1'),
	(14, 'PeptoGel', 'PeptoGel', 'Medicamento que actúa en el estómago, formando una capa protectora que cubre la mucosa. Alivia la acidez, la indigestión, diarrea y gastritis.', 12000, 'images/Antiacidos/PeptoGel.jpg', '2020-10-22', 1, 1, 40, 2, b'1'),
	(15, 'Cetirizina', 'Cetirizina', 'Selectivo. Inhibe la fase inicial de la reacción alérgica, y reduce la migración de células inflamatorias y la liberación de mediadores asociados a respuesta tardía.', 5600, 'images/Antialérgicos/Cetirizina.jpg', '2020-10-29', 1, 1, 47, 3, b'1'),
	(16, 'Fexofenadina', 'Fexofenadina', 'Medicamento que se usa para tratar ciertos síntomas alérgicos.', 7600, 'images/Antialérgicos/Fexofenadina.png', '2020-11-20', 1, 1, 0, 3, b'1'),
	(17, 'Loratadina', 'Loratadina', 'La loratadina difiere químicamente de otras antihistaminas H1 con propiedades no sedantes como son el astemizol o la terfenadina.', 4500, 'images/Antialérgicos/Loratadina.jpg', '2020-11-20', 1, 1, 30, 3, b'1'),
	(18, 'Olopatadina', 'Olopatadina', 'Actúa al prevenir la liberación de sustancias que causan picazón en los ojos', 9800, 'images/Antialérgicos/Olopatadina.jpg', '2021-01-20', 1, 1, 30, 3, b'1'),
	(19, 'Prednisona', 'Prednisona', 'Es usado para un gran número de afecciones,como asma, gripe,fiebre, tos y para la bilis. Tiene principalmente un efecto glucocorticoide.', 7600, 'images/Antialérgicos/Prednisona.jpg', '2021-01-21', 1, 1, 33, 3, b'1'),
	(20, 'Smecta', 'Smecta', 'Es un polvo para suspensión oral en sobres indicada en tratamiento sintomático de: – diarrea aguda en niños y bebés además de la rehidratación oral y en los adultos;', 3000, 'images/Antidiarreicos/Smecta.jpg', '2020-12-18', 1, 1, 23, 4, b'1'),
	(21, 'Esomeprazol', 'Esomeprazol', 'Reduce la secreción del ácido gástrico al inhibir la ATPasa de la membrana celular de las células parietales del estómago.', 5600, 'images/Antidiarreicos/Esomeprazol.jpg', '2020-10-29', 1, 1, 23, 4, b'1'),
	(22, 'Loperamida', 'Loperamida', 'Se trata de un fármaco efectivo contra la diarrea generada por una gastroenteritis o una enfermedad inflamatoria intestinal.', 9200, 'images/Antidiarreicos/Loperamida.jpg', '2020-10-15', 1, 1, 25, 4, b'1'),
	(23, 'Suero Oral en Polvo', 'Suero Oral en Polvo', 'Es una fórmula en polvo que favorece la reposición de líquidos en forma inmediata evitando la deshidratación ya que contiene sales de sodio, potasio, cloro, citrato, además de glucosa, vitales para el buen funcionamiento del organismo, sobre todo, du', 6500, 'images/Antidiarreicos/Polvo.jpg', '2020-10-29', 1, 1, 30, 4, b'1'),
	(25, 'Suero Oral', 'Suero Oral', 'Contiene sales de sodio, potasio, cloro, citrato, además de glucosa, vitales para el buen funcionamiento del organismo, sobre todo, durante una enfermedad aguda diarreica.', 4600, 'images/Antidiarreicos/Suero Oral.png', '2023-05-10', 1, 1, 28, 4, b'1'),
	(26, 'Amoxicilina', 'Amoxicilina', 'Es bactericida y actúa inhibiendo la biosíntesis del mucopéptido de la pared celular bacteriana.', 3500, 'images/Antiinfeciosos/Amoxixilina.jpg', '2020-10-15', 1, 1, 16, 5, b'1'),
	(27, 'Cefalexina', 'Cefalexina', 'Es utilizado para tratar infecciones bacterianas en el tracto respiratorio (neumonía, faringitis), la piel, los huesos, el oído (otitis media).\r\n', 13000, 'images/Antiinfeciosos/Cefalexina.jpg', '2020-10-30', 1, 1, 48, 5, b'1'),
	(28, 'Ciprofloxacino', 'Ciprofloxacino', 'Su modo de acción consiste en paralizar la replicación bacterial del ADN al unirse con una enzima llamada ADN girasa, que queda bloqueada.', 5500, 'images/Antiinfeciosos/Ciproflozacino.jpg', '2020-10-30', 1, 1, 50, 5, b'1'),
	(29, 'Clotrimazol', 'Clotrimazol', 'Es un medicamento antimicótico comúnmente usado para el tratamiento de infecciones (de humanos como de otros animales) tales como las infecciones vaginales por levaduras, candidiasis oral, y dermatofitosis (tiña).', 12000, 'images/Antiinfeciosos/Clotrimazol.jpg', '2020-10-30', 1, 1, 40, 5, b'1'),
	(30, 'Oxacilina', 'Oxacilina', 'Es un antibiótico betalactámico, de espectro reducido del grupo de las penicilinas, por lo que se indica en el tratamiento de infecciones causadas por bacterias Gram positivas, en particular las especies de estafilococos que suelen ser resistentes a otr', 8700, 'images/Antiinfeciosos/Oxacilina.jpg', '2020-10-30', 1, 1, 50, 5, b'1'),
	(31, 'Beclometazona', 'Beclometazona', 'Es un glucocorticoide sintético halogenado activo por inhalación, que se utiliza para el tratamiento del asma que responde a los glucocorticoides y para aliviar los síntomas asociados a la rinitis alérgica y no alérgica.', 4500, 'images/Antinflamatorios/Beclometasona.jpg', '2020-10-30', 1, 1, 30, 6, b'1'),
	(32, 'Meloxicam', 'Meloxicam', 'Es un antiinflamatorio no esteroide (AINE, NSAID por sus siglas en inglés) que se usa para el tratamiento del dolor o inflamación causados por la osteoartritis o la artritis reumatoide. ', 4500, 'images/Antinflamatorios/Meloxicam.jpg', '2020-10-30', 1, 1, 40, 6, b'1'),
	(33, 'Wassertrol', 'Wassertrol', 'En este producto la combinación de la respuesta anti-inflamatoria de la dexametasona con la acción antibacterial de amplio espectro de la Neomicina y la Polimixina B Sulfato, presenta una excelente respuesta terapéutica para el manejo de infecciones oc', 6500, 'images/Antinflamatorios/Wassertrol.jpg', '2020-10-30', 1, 1, 50, 6, b'1'),
	(34, 'Naproxeno', 'Naproxeno', 'Es un medicamento antinflamatorio no esteroideo que se emplea en el tratamiento del dolor leve a moderado, la fiebre, la inflamación y la rigidez provocados por afecciones como artrosis, artritis psoriásica, espondilitis anquilosante, tendinitis y bursi', 6500, 'images/Antinflamatorios/Naproxeno.jpg', '2020-10-30', 1, 1, 40, 6, b'1'),
	(36, 'Bisolvon', 'Bisolvon', 'La bromhexina, principio activo de este medicamento, pertenece a los llamados mucolíticos que actúan disminuyendo la viscosidad de las secreciones mucosas, fluidificándolas y facilitando su eliminación.', 13000, 'images/mucolitico/bisolvon.jpg', '2020-10-30', 1, 1, 50, 8, b'1'),
	(37, 'Angileptol', 'Angileptol', 'Está indicado para combatir los dolores de garganta leves.', 4500, 'images/mucolitico/Angileptol.jpg', '2020-10-31', 1, 1, 40, 8, b'1'),
	(38, 'Carbocisteina', 'Carbocisteina', 'Es un fármaco expectorante que se prescribe para procesos bronquiales donde al paciente le sea difícil expulsar la flema.', 4500, 'images/mucolitico/Carbocisteina.jpg', '2020-10-30', 1, 1, 40, 8, b'1'),
	(39, 'Pañales', 'Pañales winny', 'Pañales Etapa 3', 12000, 'images/Bebes/pañales.jpg', '2020-10-30', 1, 1, 47, 9, b'1'),
	(40, 'Leche', 'Leche', 'Leche entera en polvo', 34000, 'images/Bebes/Leche.jpg', '2020-10-30', 1, 1, 27, 9, b'1'),
	(41, 'Crema Antipañalitis', 'Crema Antipañalitis', 'Crema Antipañalitis', 23000, 'images/Bebes/Crema.jpg', '2020-10-30', 1, 1, 30, 9, b'1');
/*!40000 ALTER TABLE `medicamentos` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.orden_medicamentos
CREATE TABLE IF NOT EXISTS `orden_medicamentos` (
  `id_orden_medicamento` int(11) NOT NULL AUTO_INCREMENT,
  `id_medicamento` int(11) NOT NULL,
  `relacion` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  PRIMARY KEY (`id_orden_medicamento`),
  KEY `relacion` (`relacion`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.orden_medicamentos: ~47 rows (aproximadamente)
/*!40000 ALTER TABLE `orden_medicamentos` DISABLE KEYS */;
REPLACE INTO `orden_medicamentos` (`id_orden_medicamento`, `id_medicamento`, `relacion`, `cantidad`) VALUES
	(1, 3, '18604a92-ba24-4f2c-9e6b-5e80b8ce6aa2', 5),
	(2, 4, '18604a92-ba24-4f2c-9e6b-5e80b8ce6aa2', 3),
	(3, 10, 'f835e4bb-cd31-48c0-a0c6-43ca60c5effd', 2),
	(4, 11, 'f835e4bb-cd31-48c0-a0c6-43ca60c5effd', 2),
	(5, 12, 'f835e4bb-cd31-48c0-a0c6-43ca60c5effd', 2),
	(6, 36, 'cb8f8d66-2a72-4da9-aef3-22d8e3049c44', 10),
	(7, 37, 'cb8f8d66-2a72-4da9-aef3-22d8e3049c44', 5),
	(8, 3, 'b828bece-ae56-4ffa-b338-34ef4b4a33ad', 85),
	(9, 3, 'e87cf845-e444-4b57-80d3-3bbf8ce3db0b', 50),
	(10, 4, 'e87cf845-e444-4b57-80d3-3bbf8ce3db0b', 46),
	(11, 3, '5c2ea861-f94b-4a68-a3ea-79d5bb6f374e', 10),
	(12, 3, '8dcceed3-d2fb-4cd0-bb58-779f3973c5e9', 10),
	(13, 3, '9975a9bf-4324-4e1c-9cb0-b9c51f46b868', 10),
	(14, 3, '35f1e304-0108-433b-a52a-6d66df327ed7', 20),
	(15, 3, 'a983c149-af2a-454f-914f-4f74010f383c', 30),
	(16, 3, '28d948b9-9831-4ade-9f17-d686cdc465d7', 42),
	(17, 4, '47384547-1a80-49c7-b06e-2cf2a904e475', 50),
	(18, 3, '47384547-1a80-49c7-b06e-2cf2a904e475', 28),
	(19, 3, 'b57ae852-a5aa-4eb2-80ca-db8255ffa698', 50),
	(20, 4, 'b57ae852-a5aa-4eb2-80ca-db8255ffa698', 20),
	(21, 3, '6daffece-efd4-4703-a00e-6ecec7506f39', 52),
	(22, 3, '7095749a', 20),
	(23, 4, '7095749a', 20),
	(24, 6, '7095749a', 20),
	(25, 3, '4005cbfe-4d19-4cd9-873e-7462bc231a61', 10),
	(26, 16, 'e2dff43b-a4f6-46c4-b651-8caea8416174', 40),
	(27, 3, '3986edd6-5647-4bbd-a1e7-3dcc5f1b1bed', 26),
	(28, 11, '64574185-5a36-4ab7-908f-78a8af40d078', 4),
	(29, 4, '36390603-b657-4290-86d8-d7762aebb0e6', 2),
	(30, 3, '36390603-b657-4290-86d8-d7762aebb0e6', 3),
	(31, 3, '6d7cd65a-d6fd-4886-829f-c590f6f56f31', 2),
	(32, 10, '8920e16d-9ac4-4971-8185-86a4d678ba14', 3),
	(33, 40, '976a5b00-6bfb-46d1-9662-630cdbb7f8b0', 3),
	(34, 39, '9fdfc8e8-4891-4b44-8741-6e9754359751', 3),
	(35, 3, '0539d653-bf71-4a95-a650-e7cdce3bbef8', 2),
	(36, 3, '580d0bbc-ab43-493a-8002-2a11671e09d6', 2),
	(37, 26, 'e2671059-1213-4dd5-b3df-7f5cc1e1675a', 2),
	(38, 27, 'e2671059-1213-4dd5-b3df-7f5cc1e1675a', 2),
	(39, 3, 'e5f01b5b-b355-488c-9a79-1a80ed93b217', 1),
	(40, 3, '53d9e1c3-b99c-4e2b-80e0-b8d6ed845a7f', 5),
	(41, 26, 'f0f9d55e-a92b-41c4-9494-dcde23eef509', 2),
	(42, 3, 'be90f14f-7548-4ca6-94d3-79ac4ebc0834', 2),
	(43, 3, 'dd55ad92-f4c4-41a3-8f5c-8b2e04ff0d20', 3),
	(44, 4, 'f02f5095-c437-4471-94ba-3228f92bacda', 1),
	(45, 3, 'f02f5095-c437-4471-94ba-3228f92bacda', 1),
	(46, 15, '40102bb5-ff14-4346-ad42-292a9d0d0c8c', 2),
	(47, 4, 'c93b2614-8730-4387-9a99-412a2793ec37', 2),
	(48, 3, 'c93b2614-8730-4387-9a99-412a2793ec37', 5);
/*!40000 ALTER TABLE `orden_medicamentos` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.orden_pedidos
CREATE TABLE IF NOT EXISTS `orden_pedidos` (
  `id_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `valor_pedido` double NOT NULL DEFAULT 0,
  `valor_domicilio` double(22,0) NOT NULL DEFAULT 8000,
  `cod_cliente` varchar(50) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  `fecha_ped` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `observaciones` tinytext DEFAULT NULL,
  `relacion` varchar(50) NOT NULL,
  `entrega` int(11) DEFAULT 0,
  `codigo_pedido` varchar(50) DEFAULT NULL,
  `estado_entrega` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_pedido`),
  KEY `relacion` (`relacion`),
  CONSTRAINT `FK_orden_pedidos_orden_medicamentos` FOREIGN KEY (`relacion`) REFERENCES `orden_medicamentos` (`relacion`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.orden_pedidos: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `orden_pedidos` DISABLE KEYS */;
REPLACE INTO `orden_pedidos` (`id_pedido`, `valor_pedido`, `valor_domicilio`, `cod_cliente`, `estado`, `fecha_ped`, `observaciones`, `relacion`, `entrega`, `codigo_pedido`, `estado_entrega`) VALUES
	(4, 40000, 8000, '2', b'0', '2020-10-31 03:06:03', 'ghjkghj', 'f835e4bb-cd31-48c0-a0c6-43ca60c5effd', 0, '6e7-ec1-c01-732', 1),
	(5, 40000, 8000, '2', b'0', '2020-10-31 01:52:09', '222222', 'f835e4bb-cd31-48c0-a0c6-43ca60c5effd', 1, 'a1a-f39-f38-d31', 1),
	(6, 40000, 8000, '2', b'1', '2020-10-30 20:57:52', NULL, 'f835e4bb-cd31-48c0-a0c6-43ca60c5effd', 0, NULL, 1),
	(7, 42000, 8000, '2', b'1', '2020-10-30 20:57:53', NULL, '28d948b9-9831-4ade-9f17-d686cdc465d7', 0, NULL, 1),
	(8, 148000, 8000, '2', b'1', '2020-10-30 20:57:54', NULL, '47384547-1a80-49c7-b06e-2cf2a904e475', 0, NULL, 1),
	(9, 98000, 8000, '2', b'0', '2020-10-31 01:23:41', 'prueba', 'b57ae852-a5aa-4eb2-80ca-db8255ffa698', 1, '79a-0b9-99a-0b1', 1),
	(10, 52000, 8000, '2', b'0', '2020-10-31 02:54:31', 'hola', '6daffece-efd4-4703-a00e-6ecec7506f39', 1, '89a-0b9-99a-0b1', 1),
	(11, 148000, 8000, '2', b'1', '2020-10-31 01:27:32', NULL, '7095749a', 0, NULL, 1),
	(12, 10000, 8000, '2', b'0', '2020-10-31 01:27:01', 'ghfjfghjfghj', '4005cbfe-4d19-4cd9-873e-7462bc231a61', 1, '918-e88-658-1cf', 1),
	(13, 304000, 8000, '2', b'0', '2020-10-31 01:28:10', 'fghdfgh', 'e2dff43b-a4f6-46c4-b651-8caea8416174', 1, '2b2-938-dc1-7c8', 1),
	(14, 26000, 8000, '2', b'0', '2020-10-31 13:57:37', '', '3986edd6-5647-4bbd-a1e7-3dcc5f1b1bed', 0, '3fe-327-6f7-69e', 0),
	(15, 39200, 8000, '2', b'1', '2020-10-31 14:35:10', NULL, '64574185-5a36-4ab7-908f-78a8af40d078', 0, NULL, 1),
	(23, 102000, 8000, '', b'0', '2020-10-31 15:48:05', '', '976a5b00-6bfb-46d1-9662-630cdbb7f8b0', 0, '491-ea7-a74-a00', 1),
	(25, 2000, 8000, '', b'0', '2020-10-31 15:48:03', '', '0539d653-bf71-4a95-a650-e7cdce3bbef8', 0, 'bb8-633-63a-44e', 1),
	(26, 2000, 8000, '580d0bbc-ab43-493a-8002-2a11671e09d6', b'0', '2020-10-31 15:25:44', 'hola', '580d0bbc-ab43-493a-8002-2a11671e09d6', 0, '057-626-e8c-877', 0),
	(27, 33000, 8000, 'e2671059-1213-4dd5-b3df-7f5cc1e1675a', b'0', '2020-10-31 15:48:01', '', 'e2671059-1213-4dd5-b3df-7f5cc1e1675a', 0, 'ad8-64e-af1-f28', 1),
	(28, 1000, 8000, 'e5f01b5b-b355-488c-9a79-1a80ed93b217', b'0', '2020-10-31 15:48:10', '', 'e5f01b5b-b355-488c-9a79-1a80ed93b217', 0, '035-504-a3b-e4b', 1),
	(29, 5000, 8000, '53d9e1c3-b99c-4e2b-80e0-b8d6ed845a7f', b'0', '2020-10-31 15:33:21', 'wewe', '53d9e1c3-b99c-4e2b-80e0-b8d6ed845a7f', 0, '4f0-ac2-c05-735', 1),
	(30, 7000, 8000, 'f0f9d55e-a92b-41c4-9494-dcde23eef509', b'0', '2020-10-31 15:47:58', 'reerre', 'f0f9d55e-a92b-41c4-9494-dcde23eef509', 0, 'f53-e79-8af-f86', 1),
	(31, 2000, 8000, 'be90f14f-7548-4ca6-94d3-79ac4ebc0834', b'0', '2020-10-31 15:47:21', '', 'be90f14f-7548-4ca6-94d3-79ac4ebc0834', 0, 'f40-4aa-df5-cae', 1),
	(32, 3000, 8000, 'dd55ad92-f4c4-41a3-8f5c-8b2e04ff0d20', b'0', '2020-10-31 17:38:01', 'prueba', 'dd55ad92-f4c4-41a3-8f5c-8b2e04ff0d20', 0, '08e-fd9-812-64d', 0),
	(33, 3400, 8000, 'f02f5095-c437-4471-94ba-3228f92bacda', b'0', '2020-10-31 15:54:25', 'prueba final', 'f02f5095-c437-4471-94ba-3228f92bacda', 0, '19a-6e9-498-7c6', 0),
	(34, 11200, 8000, '40102bb5-ff14-4346-ad42-292a9d0d0c8c', b'0', '2020-10-31 17:37:59', 'qqqq', '40102bb5-ff14-4346-ad42-292a9d0d0c8c', 0, '238-531-800-a1a', 0),
	(35, 9800, 8000, 'c93b2614-8730-4387-9a99-412a2793ec37', b'0', '2020-10-31 16:27:05', 'Prueba', 'c93b2614-8730-4387-9a99-412a2793ec37', 0, '28b-96d-7d4-ba4', 0);
/*!40000 ALTER TABLE `orden_pedidos` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.ped_empleado
CREATE TABLE IF NOT EXISTS `ped_empleado` (
  `id_ped_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` varchar(50) NOT NULL DEFAULT '0',
  `id_empleado` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_ped_empleado`),
  KEY `id_empleado` (`id_empleado`),
  KEY `codigo_ped` (`id_pedido`) USING BTREE,
  CONSTRAINT `FK_ped_empleado_empleados` FOREIGN KEY (`id_empleado`) REFERENCES `empleados` (`id_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.ped_empleado: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `ped_empleado` DISABLE KEYS */;
REPLACE INTO `ped_empleado` (`id_ped_empleado`, `id_pedido`, `id_empleado`) VALUES
	(2, '10', 11),
	(3, '10', 2);
/*!40000 ALTER TABLE `ped_empleado` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.tipo_empleados
CREATE TABLE IF NOT EXISTS `tipo_empleados` (
  `id_tipo_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL,
  `estado` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_tipo_empleado`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.tipo_empleados: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_empleados` DISABLE KEYS */;
REPLACE INTO `tipo_empleados` (`id_tipo_empleado`, `descripcion`, `estado`) VALUES
	(1, 'domiciliario', b'1'),
	(2, 'farmaceutico', b'1');
/*!40000 ALTER TABLE `tipo_empleados` ENABLE KEYS */;

-- Volcando estructura para tabla medi_web.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `contrasena` varchar(50) NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `subtipo_usuario` int(11) NOT NULL,
  `documento` varchar(50) NOT NULL,
  KEY `Índice 1` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla medi_web.usuarios: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
REPLACE INTO `usuarios` (`id_usuario`, `usuario`, `contrasena`, `tipo_usuario`, `subtipo_usuario`, `documento`) VALUES
	(1, 'Felipe', '123456789', 2, 2, '100'),
	(2, 'fghjfgh', 'fghjfghj', 2, 1, '8012147'),
	(3, 'aacosta', '123', 2, 2, '123456789'),
	(4, 'maria', '123', 2, 2, '147258369'),
	(5, 'prueba', '123', 2, 1, '123');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
